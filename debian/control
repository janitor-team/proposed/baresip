Source: baresip
Section: comm
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 help2man,
 libasound2-dev,
 libavcodec-dev,
 libavdevice-dev,
 libavfilter-dev,
 libavformat-dev,
 libavutil-dev,
 libcairo2-dev,
 libcodec2-dev,
 libdirectfb-dev [!hurd-any !kfreebsd-any],
 libevdev-dev [linux-any],
 libgsm1-dev,
 libgstreamer-plugins-base1.0-dev,
 libgstreamer1.0-dev,
 libgtk2.0-dev,
 libjack-dev,
 libmosquitto-dev,
 libmp3lame-dev,
 libmpg123-dev,
 libomxil-bellagio-dev,
 libopenaptx-dev,
 libopencore-amrnb-dev,
 libopencore-amrwb-dev,
 libopus-dev,
 libpng-dev,
 libpulse-dev,
 libre-dev (>= 1.0.0),
 librem-dev (>= 0.6.0),
 libsdl2-dev,
 libsndfile1-dev,
 libsndio-dev,
 libspandsp-dev,
 libspeexdsp-dev,
 libssl-dev,
 libswresample-dev,
 libswscale-dev,
 libtwolame-dev,
 libvo-amrwbenc-dev,
 libvpx-dev,
 libx11-dev,
 libx265-dev,
 libxext-dev,
 pkg-config,
 portaudio19-dev,
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
 Vasudev Kamath <vasudev@copyninja.info>,
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/pkg-voip-team/baresip.git
Vcs-Browser: https://salsa.debian.org/pkg-voip-team/baresip
Homepage: https://github.com/baresip/baresip
Rules-Requires-Root: no

Package: baresip
Architecture: all
Depends:
 baresip-core,
 ${misc:Depends},
Recommends:
 baresip-ffmpeg,
 baresip-gstreamer,
 baresip-gtk,
 baresip-x11,
Description: portable and modular SIP user-agent - metapackage
 A modular SIP user-agent
 with support for audio and video, and many IETF standards
 such as SIP, SDP, RTP/RTCP, STUN, TURN, ICE, and WebRTC.
 .
 Supports both IPv4 and IPv6, and the following features.
  * Audio codecs: AMR, aptX, EBU ACIP, G.711, G.722, G.726, GSM,
    L16, MPA, OPUS.
  * Video codecs: H.263, H.264, H.265, MPEG4, VP8, VP9.
  * Audio drivers: Alsa, GStreamer, JACK, OSS,
    Portaudio, PulseAudio, sndio.
  * Video sources: FFmpeg avformat, Video4Linux2, X11 Grabber.
  * Video outputs: SDL2, X11, DirectFB.
  * NAT Traversal: STUN, TURN, ICE, NAT-PMP.
  * Media encryption: SRTP, DTLS-SRTP.
  * Telemetry messaging: MQTT.
  * Control interfaces: JSON-over-TCP.
 .
 This metapackage will install baresip and all its optional features.

Package: baresip-core
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 baresip-ffmpeg,
 baresip-gstreamer,
 baresip-gtk,
 baresip-x11,
Description: portable and modular SIP user-agent - core parts
 A modular SIP user-agent
 with support for audio and video, and many IETF standards
 such as SIP, SDP, RTP/RTCP, STUN, TURN, ICE, and WebRTC.
 .
 Supports both IPv4 and IPv6, and the following features.
  * Audio codecs: AMR, aptX, EBU ACIP, G.711, G.722, G.726, GSM,
    L16, MPA, OPUS.
  * Video codecs: H.263, H.264, H.265, MPEG4, VP8, VP9.
  * Audio drivers: Alsa, GStreamer, JACK, OSS,
    Portaudio, PulseAudio, sndio.
  * Video sources: FFmpeg avformat, Video4Linux2, X11 Grabber.
  * Video outputs: SDL2, X11, DirectFB.
  * NAT Traversal: STUN, TURN, ICE, NAT-PMP.
  * Media encryption: SRTP, DTLS-SRTP.
  * Telemetry messaging: MQTT.
  * Control interfaces: JSON-over-TCP.
 .
 Some of above features are provided in separate packages
 baresip-gtk, baresip-ffmpeg, baresip-gstreamer and baresip-x11.

Package: baresip-gtk
Architecture: any
Depends:
 baresip-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Enhances:
 baresip-core,
Description: portable and modular SIP user-agent - GTK+ front-end
 A modular SIP user-agent
 with support for audio and video, and many IETF standards
 such as SIP, SDP, RTP/RTCP, STUN, TURN, ICE, and WebRTC.
 .
 Supports both IPv4 and IPv6, and the following features.
  * Audio codecs: AMR, aptX, EBU ACIP, G.711, G.722, G.726, GSM,
    L16, MPA, OPUS.
  * Video codecs: H.263, H.264, H.265, MPEG4, VP8, VP9.
  * Audio drivers: Alsa, GStreamer, JACK, OSS,
    Portaudio, PulseAudio, sndio.
  * Video sources: FFmpeg avformat, Video4Linux2, X11 Grabber.
  * Video output: SDL2, X11, DirectFB.
  * NAT Traversal: STUN, TURN, ICE, NAT-PMP.
  * Media encryption: SRTP, DTLS-SRTP.
  * Telemetry messaging: MQTT.
  * Control interfaces: JSON-over-TCP.
 .
 This package provides a GTK+ front-end for baresip.

Package: baresip-ffmpeg
Architecture: any
Depends:
 baresip-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Enhances:
 baresip-core,
Description: portable and modular SIP user-agent - FFmpeg codecs and formats
 A modular SIP user-agent
 with support for audio and video, and many IETF standards
 such as SIP, SDP, RTP/RTCP, STUN, TURN, ICE, and WebRTC.
 .
 Supports both IPv4 and IPv6, and the following features.
  * Audio codecs: AMR, aptX, EBU ACIP, G.711, G.722, G.726, GSM,
    L16, MPA, OPUS.
  * Video codecs: H.263, H.264, H.265, MPEG4, VP8, VP9.
  * Audio drivers: Alsa, GStreamer, JACK, OSS,
    Portaudio, PulseAudio, sndio.
  * Video sources: FFmpeg avformat, Video4Linux2, X11 Grabber.
  * Video outputs: SDL2, X11, DirectFB.
  * NAT Traversal: STUN, TURN, ICE, NAT-PMP.
  * Media encryption: SRTP, DTLS-SRTP.
  * Telemetry messaging: MQTT.
  * Control interfaces: JSON-over-TCP.
 .
 This package integrates FFmpeg codecs and formats with baresip.

Package: baresip-gstreamer
Architecture: any
Depends:
 baresip-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Enhances:
 baresip-core,
Description: portable and modular SIP user-agent - GStreamer pipelines
 A modular SIP user-agent
 with support for audio and video, and many IETF standards
 such as SIP, SDP, RTP/RTCP, STUN, TURN, ICE, and WebRTC.
 .
 Supports both IPv4 and IPv6, and the following features.
  * Audio codecs: AMR, aptX, EBU ACIP, G.711, G.722, G.726, GSM,
    L16, MPA, OPUS.
  * Video codecs: H.263, H.264, H.265, MPEG4, VP8, VP9.
  * Audio drivers: Alsa, GStreamer, JACK, OSS,
    Portaudio, PulseAudio, sndio.
  * Video sources: FFmpeg avformat, Video4Linux2, X11 Grabber.
  * Video outputs: SDL2, X11, DirectFB.
  * NAT Traversal: STUN, TURN, ICE, NAT-PMP.
  * Media encryption: SRTP, DTLS-SRTP.
  * Telemetry messaging: MQTT.
  * Control interfaces: JSON-over-TCP.
 .
 This package integrates GStreamer pipelines with baresip.

Package: baresip-x11
Architecture: any
Depends:
 baresip-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Enhances:
 baresip-core,
Description: portable and modular SIP user-agent - X11 features
 A modular SIP user-agent
 with support for audio and video, and many IETF standards
 such as SIP, SDP, RTP/RTCP, STUN, TURN, ICE, and WebRTC.
 .
 Supports both IPv4 and IPv6, and the following features.
  * Audio codecs: AMR, aptX, EBU ACIP, G.711, G.722, G.726, GSM,
    L16, MPA, OPUS.
  * Video codecs: H.263, H.264, H.265, MPEG4, VP8, VP9.
  * Audio drivers: Alsa, GStreamer, JACK, OSS,
    Portaudio, PulseAudio, sndio.
  * Video sources: FFmpeg avformat, Video4Linux2, X11 Grabber.
  * Video outputs: SDL2, X11, DirectFB.
  * NAT Traversal: STUN, TURN, ICE, NAT-PMP.
  * Media encryption: SRTP, DTLS-SRTP.
  * Telemetry messaging: MQTT.
  * Control interfaces: JSON-over-TCP.
 .
 This package provides various X11-related features for baresip.
